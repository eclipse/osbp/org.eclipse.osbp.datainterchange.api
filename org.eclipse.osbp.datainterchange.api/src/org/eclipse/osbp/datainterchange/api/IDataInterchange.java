/*
 *                                                                            
 *  Copyright (c) 2011 - 2017 - Loetz GmbH & Co KG, 69115 Heidelberg, Germany 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG                              
 * 
 */
package org.eclipse.osbp.datainterchange.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;

import javax.persistence.EntityManager;

import org.osgi.framework.Bundle;
import org.xml.sax.SAXException;

public interface IDataInterchange {
	
	/**
	 * Open the instance.
	 *
	 * @param bundle the bundle
	 * @param configFilename the config filename
	 * @return Smooks instance
	 */
	Object open(Bundle bundle, String configFilename) throws IOException, SAXException;
	
	/**
	 * Sets the entity manager.
	 *
	 * @param smooks the smooks instance
	 * @param entityManager the entity manager
	 */
	void setEntityManager(Object smooks, EntityManager entityManager);
	
	/**
	 * Enable report.
	 *
	 * @param smooks the smooks instance
	 * @param location the location of the report file.
	 */
	void enableReport(Object smooks, String location);
	
	/**
	 * Sets the event listener.
	 *
	 * @param listener the new event listener
	 */
	void setEventListener(IDataInterchangeExecutionEventListener listener);
	
	/**
	 * Open stream.
	 *
	 * @param fileURL the file URL
	 * @return the input stream
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	InputStream openStream(URL fileURL) throws IOException;

    /**
     * import the source and extract the given bean as object.
     *
     * @param smooks the smooks instance
     * @param source the source
     * @param beanName the bean name
     * @return the object
     * @throws DataInterchangeException the data interchange exception
     */
    Object importSource(Object smooks, InputStream source, String beanName);
    
    /**
     * Import source.
     *
     * @param smooks the smooks instance
     * @param source the source
     * @param beanName the bean name
     * @param encoding encoding of input file e.g. UTF-8
     * @return the object
     */
    Object importSource(Object smooks, InputStream source, String beanName, String encoding);
    
	/**
	 * Export the source through the writer.
	 *
	 * @param smooks the smooks instance
	 * @param source the source
	 * @param writer the writer
	 */
	void exportSource(Object smooks, Object source, StringWriter writer);

	/**
	 * Close the instance and all associated resources.
	 *
	 * @param smooks the smooks instance
	 */
	void close(Object smooks);
}
