/*
 *                                                                            
 *  Copyright (c) 2011 - 2017 - Loetz GmbH & Co KG, 69115 Heidelberg, Germany 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG                              
 * 
 */
package org.eclipse.osbp.datainterchange.api;

public class DataInterchangeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new data interchange exception.
	 *
	 * @param message the message
	 */
	public DataInterchangeException(String message) {
        super(message);
    }

	/**
	 * Instantiates a new data interchange exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public DataInterchangeException(String message, Throwable cause) {
		super(message, cause);
	}
}
